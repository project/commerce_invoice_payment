<?php

namespace Drupal\commerce_invoice_payment\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a pane for Checkout complete page.
 *
 * @CommerceCheckoutPane(
 *   id = "comemrce_invoice_payment_order_order_complete_pane",
 *   label = @Translation("Invoice Payment order complete"),
 * )
 */
class InvoicePaymentOrderCompletePane extends CheckoutPaneBase {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new PaymentProcess object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, RequestStack $request_stack, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $order = $this->order;
    if ($order->isPaid()) {
      $invoices = [];
      foreach ($order->getItems() as $order_item) {
        $invoice = $order_item->getPurchasedEntity();
        $invoices[] = $invoice->label();
      }
      $pane_form['invoice'] = [
        '#theme' => 'item_list',
        '#items' => $invoices,
        '#title' => $this->t('You have successfully paid the following invoices:'),
      ];
    }
    $commerce_invoice_payment_data = $order->getData('commerce_invoice_payment');
    if (!empty($commerce_invoice_payment_data['entry_url'])) {
      $title = !empty($commerce_invoice_payment_data['entry_page_title']) ? $commerce_invoice_payment_data['entry_page_title'] : $this->t('Invoice overview');
      $pane_form['return_back_link'] = [
        '#type' => 'link',
        '#title' => $title,
        '#url' => Url::fromUserInput($commerce_invoice_payment_data['entry_url']),
      ];
    }

    return $pane_form;
  }

}
