<?php

namespace Drupal\commerce_invoice_payment\Plugin\Action;

use Drupal\commerce_price\Calculator;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/** Action to pay invoices.
 *
 * @Action(
 *   id = "commerce_invoice_payment_pay_invoice_action",
 *   label = @Translation("Pay invoice"),
 *   type = "commerce_invoice",
 *   confirm = TRUE
 * )
 */
class PayInvoiceAction extends ViewsBulkOperationsActionBase  implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RequestStack $request_stack
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    foreach ($entities as $entity) {
      $this->execute($entity);
    }
    $order_id = $this->getOrderId();

    if (!empty($order_id)) {
      $this->context['results']['redirect_url'] = Url::fromRoute('commerce_checkout.form', [
        'commerce_order' => $order_id,
        'step' => 'review',
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $orderStorage = $this->entityTypeManager->getStorage('commerce_order');
    $orderItemStorage = $this->entityTypeManager->getStorage('commerce_order_item');

    /** @var \Drupal\commerce_invoice\Entity\InvoiceInterface $invoice */
    $invoice = $entity;
    /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
    $order_item = $orderItemStorage->createFromPurchasableEntity($invoice);
    $order_id = $this->getOrderId();
    if (empty($order_id)) {
      $order = $orderStorage->create([
        'type' => 'invoice_payment',
        'mail' => $this->view->getUser()->getEmail(),
        'uid' => $this->view->getUser()->id(),
        'store_id' => $invoice->getStoreId(),
      ]);
      $order->setData('commerce_invoice_payment', [
        'entry_url' => $this->getEntryUrl(),
        'entry_page_title' => $this->getEntryPageTitle(),
        ]
      );
    }
    else {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      $order = $orderStorage->load($order_id);
    }
    $order->addItem($order_item);
    $order->save();
    if (empty($this->getOrderId())) {
      $this->setOrderId($order->id());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setContext(array &$context): void {
    $this->context['sandbox'] = &$context['sandbox'];
    $this->context['results'] = &$context['results'];
    foreach ($context as $key => $item) {
      if ($key === 'sandbox' || $key === 'results') {
        continue;
      }
      $this->context[$key] = $item;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $object->access('update', $account, $return_as_object) && $account->hasPermission('use pay invoice action');
  }

  /**
   * Saves order ID to context.
   *
   * @param string $order_id
   *   Order ID.
   */
  protected function setOrderId($order_id) {
    $this->context['commerce_invoice_payment']['order_id'] = $order_id;
  }

  /**
   * Gets order ID from context.
   *
   * @return string|null
   */
  protected function getOrderId() {
    return $this->context['commerce_invoice_payment']['order_id'] ?? NULL;
  }

  /**
   * Gets entry URL.
   *
   * @return string
   */
  protected function getEntryUrl() {
    if (!empty($this->context['redirect_url']) && $this->context['redirect_url'] instanceof Url) {
      return $this->context['redirect_url']->toString();
    }
    return '';
  }

  /**
   * Returns a title of the view.
   *
   * @return false|string
   */
  protected function getEntryPageTitle() {
    return $this->view->getTitle();
  }

}
