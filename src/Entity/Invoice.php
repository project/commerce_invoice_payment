<?php

namespace Drupal\commerce_invoice_payment\Entity;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_invoice\Entity\Invoice as InvoiceBase;

class Invoice extends InvoiceBase implements PurchasableEntityInterface {

  /**
   * {@inheritdoc}
   */
  public function getStores() {
    return [$this->getStore()];
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice() {
    return $this->getBalance();
  }

  /**
   * @inheritDoc
   */
  public function getOrderItemTypeId() {
    return 'invoice_payment';
  }

  /**
   * @inheritDoc
   */
  public function getOrderItemTitle() {
    return $this->getInvoiceNumber();
  }

}
