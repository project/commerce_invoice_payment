<?php

namespace Drupal\commerce_invoice_payment\EventSubscriber;

use Drupal\commerce_invoice\Entity\InvoiceInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_order\Event\OrderEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class OrderPaidSubscriber.
 */
class OrderPaidSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      OrderEvents::ORDER_PAID => 'orderPaidHandler',
    ];
  }

  /**
   * Processes the invoices.
   *
   * @param \Drupal\commerce_order\Event\OrderEvent $event
   *   The order event.
   */
  public function orderPaidHandler(OrderEvent $event) {
    $order = $event->getOrder();
    // Update invoices.
    foreach ($order->getItems() as $order_item) {
      $purchased_entity = $order_item->getPurchasedEntity();

      if (!$purchased_entity instanceof InvoiceInterface) {
        continue;
      }

      if ($purchased_entity->getState()->getId() === 'draft') {
        $purchased_entity->getState()->applyTransitionById('confirm');
      }

      $purchased_entity->getState()->applyTransitionById('pay');
      $purchased_entity->save();
    }
  }

}
